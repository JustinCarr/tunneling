package net.amigocraft.tunneling.listeners;

import java.util.HashMap;

import net.amigocraft.mglib.api.MGPlayer;
import net.amigocraft.mglib.api.Round;
import net.amigocraft.tunneling.Location2D;
import net.amigocraft.tunneling.LocationBox;
import net.amigocraft.tunneling.Main;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;

public class BlockListener implements Listener {

	@EventHandler
	@SuppressWarnings("unchecked")
	public void onBlockBreak(final BlockBreakEvent e){
		if (Main.mg.isPlayer(e.getPlayer().getName())){
			final MGPlayer mp = Main.mg.getMGPlayer(e.getPlayer().getName());
			if (e.getBlock().getType() != Material.SAND || mp.getTeam().equals("Infected")){
				e.setCancelled(true);
				return;
			}
			else if (!mp.getRound().hasMetadata("nextBarrier")){
				mp.getRound().setMetadata("nextBarrier", new HashMap<Location2D, Boolean>());
				for (int y = e.getBlock().getY(); y > 0; y--)
					if (e.getBlock().getWorld().getBlockAt(e.getBlock().getX(), y, e.getBlock().getZ()).getType() != Material.SAND){
						mp.getRound().setMetadata("barrierBase", y + 1);
						HashMap<Location2D, Boolean> barrier = ((HashMap<Location2D, Boolean>)mp.getRound().getMetadata("nextBarrier"));
						barrier.put(new Location2D(e.getBlock().getX(), e.getBlock().getZ()), true);
						mp.getRound().setMetadata("nextBarrier", barrier);
						testSurrounding(mp.getRound(), e.getBlock().getWorld().getBlockAt(e.getBlock().getX(), y + 1, e.getBlock().getZ()));
						break;
					}
				int minX = Integer.MAX_VALUE;
				int maxX = Integer.MIN_VALUE;
				int minZ = Integer.MAX_VALUE;
				int maxZ = Integer.MIN_VALUE;
				for (Location2D l : ((HashMap<Location2D, Boolean>)mp.getRound().getMetadata("nextBarrier")).keySet()){
					if (l.getX() < minX)
						minX = l.getX();
					if (l.getX() > maxX)
						maxX = l.getX();
					if (l.getZ() < minZ)
						minZ = l.getZ();
					if (l.getZ() > maxZ)
						maxZ = l.getZ();
				}
				mp.getRound().setMetadata("locationBox",
						new LocationBox(
								new Location(mp.getBukkitPlayer().getWorld(), minX, (int)mp.getRound().getMetadata("barrierBase"), minZ),
								new Location(mp.getBukkitPlayer().getWorld(), maxX, 255, maxZ), 0f));
			}
			else {
				Bukkit.getScheduler().runTask(Main.plugin, new Runnable(){
					public void run(){
						if (mp.getBukkitPlayer().getWorld().getBlockAt(
								e.getBlock().getX(), ((Integer)mp.getRound().getMetadata("barrierBase")) + 1, e.getBlock().getZ()).getType() == Material.AIR){
							HashMap<Location2D, Boolean> barrier = ((HashMap<Location2D, Boolean>)mp.getRound().getMetadata("nextBarrier"));
							barrier.put(new Location2D(e.getBlock().getX(), e.getBlock().getZ()), false);
							mp.getRound().setMetadata("nextBarrier", barrier);
						}
						boolean gone = true;
						for (boolean b : ((HashMap<Location2D, Boolean>)mp.getRound().getMetadata("nextBarrier")).values()){
							if (b){
								gone = false;
								break;
							}
						}
						if (gone){
							mp.getRound().removeMetadata("nextBarrier");
							mp.getRound().removeMetadata("barrierBase");
							mp.getRound().removeMetadata("locationBox");
						}
					}
				});
			}
			/*try {
				Class<?> c = Class.forName("net.amigocraft.mglib.MGListener");
				Method m = c.getMethod("onBlockBreak", BlockBreakEvent.class);
				m.setAccessible(true);
				m.invoke(c.newInstance(), e);
			}
			catch (IllegalAccessException | IllegalArgumentException
					| InvocationTargetException | NoSuchMethodException
					| SecurityException | ClassNotFoundException | InstantiationException ex){
				ex.printStackTrace();
			}
			e.setCancelled(true);
			e.getBlock().setType(Material.AIR);*/
			e.setCancelled(true);
			Bukkit.getScheduler().runTask(Main.plugin, new Runnable(){
				public void run(){
					e.getBlock().setType(Material.AIR);
				}
			});
		}
	}

	@SuppressWarnings("unchecked")
	private void testSurrounding(Round round, Block block){
		if (block.getType() == Material.SAND){
			((HashMap<Location2D, Boolean>)round.getMetadata("nextBarrier")).put(new Location2D(block.getX(), block.getZ()), true);
			for (int xOff = -1; xOff <= 1; xOff++)
				for (int zOff = -1; zOff <= 1; zOff++)
					if (!((HashMap<Location2D, Boolean>)round.getMetadata("nextBarrier")).containsKey(
							new Location2D(block.getX() + xOff, block.getZ() + zOff))){
						if (block.getWorld().getBlockAt(block.getX() + xOff, (int)round.getMetadata("barrierBase"),
								block.getZ() + zOff).getType() == Material.SAND){
							HashMap<Location2D, Boolean> barrier = ((HashMap<Location2D, Boolean>)round.getMetadata("nextBarrier"));
							barrier.put(new Location2D(block.getX() + xOff, block.getZ() + zOff), true);
							round.setMetadata("nextBarrier", barrier);
							testSurrounding(round, block.getWorld().getBlockAt(block.getX() + xOff, block.getY(), block.getZ() + zOff));
						}
					}
		}
	}

}
