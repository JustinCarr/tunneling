package net.amigocraft.tunneling;

public class ConfigHandler {
	
	public static int INFECTED_WAIT_TIME;
	public static double HUMAN_SPEED;
	public static double INFECTED_SPEED;
	public static double FAT_KID_SPEED;
	public static int HUMAN_JUMP;
	public static int INFECTED_JUMP;
	public static int FAT_KID_JUMP;
	public static double BARRIER_BOUNCE;
	public static int FAT_KID_BUFF;
	public static int INFECTED_BUFF;
	public static int FAT_KID_SWORD;
	public static int INFECTED_SWORD;
	public static boolean FORCE_FREEZE;
	
	public static void initialize(){
		INFECTED_WAIT_TIME = Main.plugin.getConfig().getInt("infected-wait-time");
		HUMAN_SPEED = Main.plugin.getConfig().getDouble("human-speed");
		INFECTED_SPEED = Main.plugin.getConfig().getDouble("infected-speed");
		FAT_KID_SPEED = Main.plugin.getConfig().getDouble("fat-kid-speed");
		HUMAN_JUMP = Main.plugin.getConfig().getInt("human-jump");
		INFECTED_JUMP = Main.plugin.getConfig().getInt("infected-jump");
		FAT_KID_JUMP = Main.plugin.getConfig().getInt("fat-kid-jump");
		BARRIER_BOUNCE = Main.plugin.getConfig().getDouble("barrier-bounce");
		INFECTED_BUFF = Main.plugin.getConfig().getInt("infected-buff");
		FAT_KID_BUFF = Main.plugin.getConfig().getInt("fat-kid-buff");
		FAT_KID_SWORD = Main.plugin.getConfig().getInt("fat-kid-sword");
		INFECTED_SWORD = Main.plugin.getConfig().getInt("infected-sword");
		FORCE_FREEZE = Main.plugin.getConfig().getBoolean("force-freeze");
		
	}

}
