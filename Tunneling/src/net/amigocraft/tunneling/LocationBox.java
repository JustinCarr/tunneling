package net.amigocraft.tunneling;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;

public class LocationBox {
	private Location loc1;
	private Location loc2;
	private double minX, minY, minZ, maxX, maxY, maxZ;
    private float yaw;
	private Location center;
	private Location physicalCenter;
	
	public LocationBox(Location l1, Location l2, float yaw) {
		loc1 = l1;
		loc2 = l2;
		minX = Math.min(loc1.getX(), loc2.getX());
		minY = Math.min(loc1.getBlockY(), loc2.getBlockY());
		minZ = Math.min(loc1.getZ(), loc2.getZ());

		maxX = Math.max(loc1.getX(), loc2.getX()) + 1;
		maxY = Math.max(loc1.getY(), loc2.getY()) + 1;
		maxZ = Math.max(loc1.getZ(), loc2.getZ()) + 1;
		
		double x = (maxX - minX)/2 + minX;
		double y = (maxY - minY)/2 + minY;
		double z = (maxZ - minZ)/2 + minZ;
		physicalCenter = new Location(Bukkit.getWorlds().get(0),x,y,z);
		physicalCenter.setYaw(yaw);
		this.center = physicalCenter;
	}
	
	public int getRadius() {
		double x = maxX - minX;
		double z = maxZ - minZ;
		return ((int)Math.min(x, z));
	}
	
	public void setCenter(Location l) {
		center = l;
	}
	
	/*
	 * This teleports a player out of the box towards the closest side to escape.
	 */
	public void teleportOut(Player p) {
		if (!isInsideBounds(p.getLocation())) return; //quit if not inside this box
		double x = p.getLocation().getX();
		double y = p.getLocation().getY();
		double z = p.getLocation().getZ();
		double dx = 0;
		double dz = 0;
		if (x <= physicalCenter.getX()) {
			dx = Math.abs(x) - Math.abs(minX);
		} else {
			dx = Math.abs(maxX) - Math.abs(x);
		}
		if (z <= physicalCenter.getZ()) {
			dz = Math.abs(z) - Math.abs(minZ);
		} else {
			dz = Math.abs(maxZ) - Math.abs(z);
		}
		dx = Math.abs(dx);
		dz = Math.abs(dz);
		if (dx < dz) {
			//we teleport out using the x direction
			if (x < physicalCenter.getX()) {
				x = minX - ConfigHandler.BARRIER_BOUNCE;
			} else {
				x = maxX + ConfigHandler.BARRIER_BOUNCE;
			}
		} else {
			if (z < physicalCenter.getZ()) {
				z = minZ - ConfigHandler.BARRIER_BOUNCE;
			} else {
				z = maxZ + ConfigHandler.BARRIER_BOUNCE;
			}
			//we teleport out using the z direction
		}
		Location toTeleport = new Location(Bukkit.getWorlds().get(0),x,y,z);
		toTeleport.setPitch(p.getLocation().getPitch());
		toTeleport.setYaw(p.getLocation().getYaw());
		p.teleport(toTeleport);
		p.sendMessage("A mysterious force repels you");
	}
	
	public Location getCenter() {
		return center;
	}
	
	public Location spawnInside() {
		double newY = minY + 1;
		double radiusX = (maxX - minX)/2;
		double radiusZ = (maxZ - minZ)/2;
		double newX = (maxX + minX)/2 + (Math.random()*2*radiusX - radiusX);
		double newZ = (maxZ + minZ)/2 + (Math.random()*2*radiusZ - radiusZ);
		Location l = new Location(Bukkit.getWorlds().get(0),newX,newY,newZ);
		l.setYaw(yaw);
		if (Bukkit.getWorlds().get(0).getBlockAt(l).isEmpty())
			return l;
		else
			return spawnInside();
	}
	
	public boolean isInside(Location l){
		double x = l.getX();
		double y = l.getY();
		double z = l.getZ();
		if ((x >= minX && x <= maxX) && (y >= minY && y <= maxY) && (z >=minZ && z <=maxZ)) {
			return true;
		}
		return false;
	}
	
	public boolean isInsideBounds(Location l) {
		double x = l.getX();
		double z = l.getZ();
		if ((x >= minX && x <= maxX) && (z >=minZ && z <=maxZ)) {
			return true;
		}
		return false;
	}
}
