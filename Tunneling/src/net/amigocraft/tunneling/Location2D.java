package net.amigocraft.tunneling;

public class Location2D {
	
	private int x;
	private int z;
	
	public Location2D(int x, int z){
		this.x = x;
		this.z = z;
	}
	
	public int getX(){
		return x;
	}
	
	public int getZ(){
		return z;
	}
	
	public void setX(int x){
		this.x = x;
	}
	
	public void setZ(int z){
		this.z = z;
	}
	
	public boolean equals(Object otherLocation){
		return otherLocation instanceof Location2D && this.x == ((Location2D)otherLocation).getX() && this.z == ((Location2D)otherLocation).getZ();
	}
	
	@Override
	public int hashCode(){
		return 41 * (this.x * 13 + this.z * 17 + 47);
	}

}
