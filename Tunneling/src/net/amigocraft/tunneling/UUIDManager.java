package net.amigocraft.tunneling;

import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.Callable;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.google.common.collect.ImmutableList;

public class UUIDManager implements Callable<Map<UUID, String>> {

	private static final String PROFILE_URL = "https://sessionserver.mojang.com/session/minecraft/profile/";
	
	private final JSONParser parser = new JSONParser();
	private final List<UUID> uuids;
    
    public UUIDManager(List<UUID> uuids) {
        this.uuids = ImmutableList.copyOf(uuids);
    }
 
    @Override
    public Map<UUID, String> call() throws Exception {
        
    	Map<UUID, String> uuidStringMap = new HashMap<UUID, String>();
        for (UUID uuid: uuids) {
            
        	HttpURLConnection connection = (HttpURLConnection) new URL(PROFILE_URL+uuid.toString().replace("-", "")).openConnection();
            JSONObject response = (JSONObject) parser.parse(new InputStreamReader(connection.getInputStream()));
            String name = (String) response.get("name");
            
            if (name == null) {
                continue;
            }
            
            String cause = (String) response.get("cause");
            String errorMessage = (String) response.get("errorMessage");
            
            if (cause != null && cause.length() > 0) {
                throw new IllegalStateException(errorMessage);
            }
            
            uuidStringMap.put(uuid, name);
        }
        return uuidStringMap;
    }
    
    public static UUID getUUIDOf(String pname) throws Exception {
    	List<String> names = Arrays.asList(pname);
        Map<String, UUID> uuidMap = new HashMap<String, UUID>();

        URL url = new URL("https://api.mojang.com/profiles/minecraft");
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        
        connection.setRequestMethod("POST");
        connection.setRequestProperty("Content-Type", "application/json");
        connection.setUseCaches(false);
        connection.setDoInput(true);
        connection.setDoOutput(true);
        
        String body = JSONArray.toJSONString(names);
        
        OutputStream stream = connection.getOutputStream();
        stream.write(body.getBytes());
        stream.flush();
        stream.close();
        
        JSONArray array = (JSONArray) new JSONParser().parse(new InputStreamReader(connection.getInputStream()));
        for (Object profile : array) {
            JSONObject jsonProfile = (JSONObject) profile;
            String id = (String) jsonProfile.get("id");
            String name = (String) jsonProfile.get("name");
            UUID uuid = UUID.fromString(id.substring(0, 8) + "-" + id.substring(8, 12) + "-" + id.substring(12, 16) + "-" + id.substring(16, 20) + "-" +id.substring(20, 32));

            uuidMap.put(name, uuid);
        }
        
        return uuidMap.get(pname);
    }
    
}
