package net.amigocraft.tunneling.listeners;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import me.libraryaddict.disguise.DisguiseAPI;
import me.libraryaddict.disguise.disguisetypes.DisguiseType;
import me.libraryaddict.disguise.disguisetypes.MobDisguise;
import net.amigocraft.mglib.api.MGPlayer;
import net.amigocraft.mglib.api.Round;
import net.amigocraft.mglib.api.Stage;
import net.amigocraft.mglib.event.player.MGPlayerDeathEvent;
import net.amigocraft.mglib.event.player.PlayerJoinMinigameRoundEvent;
import net.amigocraft.mglib.event.player.PlayerLeaveMinigameRoundEvent;
import net.amigocraft.mglib.event.round.MinigameRoundEndEvent;
import net.amigocraft.mglib.event.round.MinigameRoundPrepareEvent;
import net.amigocraft.mglib.event.round.MinigameRoundStartEvent;
import net.amigocraft.mglib.event.round.MinigameRoundTickEvent;
import net.amigocraft.tunneling.ConfigHandler;
import net.amigocraft.tunneling.FireworkLauncher;
import net.amigocraft.tunneling.LocationBox;
import net.amigocraft.tunneling.Main;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;

public class MGListener implements Listener {

	public static HashMap<String, Integer> timer = new HashMap<String, Integer>();

	public static List<String> violations = new ArrayList<String>();

	public static int cIndex = 0; // hehe

	public static HashMap<String, Scoreboard> scoreboards = new HashMap<String, Scoreboard>();

	public static List<String> frozen = new ArrayList<String>();

	@EventHandler
	public void onPlayerJoinRound(PlayerJoinMinigameRoundEvent e){
		e.getPlayer().getBukkitPlayer().sendMessage(ChatColor.GOLD + "[Tunneling] You have joined arena " + e.getRound().getArena() + "!");
		if (e.getRound().getPlayerList().size() <= Main.cm.getMinPlayers() && e.getRound().getTeam("Infected").size() == 0 &&
				new Random().nextInt(Main.cm.getMinPlayers() + 1 - e.getRound().getPlayerList().size()) == 0){
			e.getPlayer().setTeam("Infected");
			e.getPlayer().getBukkitPlayer().teleport(e.getRound().getSpawns().get(1));
			MobDisguise dis = new MobDisguise(DisguiseType.ZOMBIE);
			dis.getWatcher().setBurning(true);
			DisguiseAPI.disguiseToAll(e.getPlayer().getBukkitPlayer(), dis);
			e.getPlayer().setMetadata("fat-kid", true);
			e.getPlayer().getBukkitPlayer().sendMessage(ChatColor.LIGHT_PURPLE + "You have been selected as the Fat Kid!");
		}
		else {
			e.getPlayer().setTeam("Humans");
			e.getPlayer().getBukkitPlayer().teleport(e.getRound().getSpawns().get(0));
		}

		frozen.add(e.getPlayer().getName());
		e.getPlayer().getBukkitPlayer().setWalkSpeed(0f);
		e.getPlayer().getBukkitPlayer().addPotionEffect(new PotionEffect(PotionEffectType.JUMP, Integer.MAX_VALUE, 128));
		//Main.setCheckMovement(e.getPlayer().getBukkitPlayer(), false);
		if (Bukkit.getScoreboardManager().getMainScoreboard().getObjective("infected") == null)
			Bukkit.getScoreboardManager().getMainScoreboard().registerNewObjective("infected", "dummy");
		Objective obj = Bukkit.getScoreboardManager().getMainScoreboard().getObjective("infected");
		obj.getScore(e.getPlayer().getName()).setScore(e.getPlayer().getTeam() == "Infected" ? 1 : 0);

		e.getPlayer().setMetadata("kills", 0);
		updateScoreboard(e.getPlayer());
		e.getPlayer().getBukkitPlayer().setScoreboard(scoreboards.get(e.getPlayer().getName()));
	}

	@EventHandler
	public void onPlayerLeaveRound(PlayerLeaveMinigameRoundEvent e){
		e.getPlayer().getBukkitPlayer().setScoreboard(Bukkit.getScoreboardManager().getNewScoreboard());
		//Main.setCheckMovement(e.getPlayer().getBukkitPlayer(), true);
		frozen.remove(e.getPlayer().getName());
		e.getPlayer().getBukkitPlayer().setWalkSpeed(0.2f);
		DisguiseAPI.undisguiseToAll(e.getPlayer().getBukkitPlayer());
		if (e.getRound().getStage() != Stage.RESETTING){
			if (e.getRound().getAlivePlayerCount() < e.getRound().getMinPlayers() &&
					e.getRound().getStage() == Stage.PREPARING || e.getRound().getStage() == Stage.PLAYING){
				e.getRound().broadcast(ChatColor.GOLD + "[Tunneling] Not enough players to continue! Ending round...");
				e.getRound().end();
			}
			else if (e.getPlayer().hasMetadata("fat-kid")){
				Collection<MGPlayer> infected = e.getRound().getTeam("Infected").values();
				Random r = new Random();
				if (infected.size() > 0){
					int i = infected.size();
					for (MGPlayer mp : infected){
						if (r.nextInt(i) == 0){
							mp.setTeam("Infected");
							mp.setMetadata("fat-kid", true);
							e.getPlayer().getBukkitPlayer().teleport(e.getRound().getSpawns().get(1));
							MobDisguise dis = new MobDisguise(DisguiseType.ZOMBIE);
							dis.getWatcher().setBurning(true);
							DisguiseAPI.disguiseToAll(e.getPlayer().getBukkitPlayer(), dis);
							frozen.remove(e.getPlayer().getName());
							mp.getBukkitPlayer().setWalkSpeed((float)ConfigHandler.FAT_KID_SPEED * 0.2f);
							mp.getBukkitPlayer().addPotionEffect(new PotionEffect(PotionEffectType.JUMP, Integer.MAX_VALUE, ConfigHandler.FAT_KID_JUMP));
							mp.getBukkitPlayer().addPotionEffect(new PotionEffect(PotionEffectType.HEALTH_BOOST, Integer.MAX_VALUE,
									ConfigHandler.FAT_KID_BUFF));
							mp.getBukkitPlayer().setHealth(mp.getBukkitPlayer().getMaxHealth()); // replenish their health
							mp.getBukkitPlayer().sendMessage(ChatColor.AQUA + "You are now the Fat Kid!");
							break;
						}
					}
				}
				else {
					Collection<MGPlayer> humans = e.getRound().getTeam("Humans").values();
					int i = humans.size();
					for (MGPlayer mp : humans){
						if (r.nextInt(i) == 0){
							mp.setTeam("Infected");
							mp.setMetadata("fat-kid", true);
							e.getPlayer().getBukkitPlayer().teleport(e.getRound().getSpawns().get(1));
							MobDisguise dis = new MobDisguise(DisguiseType.ZOMBIE);
							dis.getWatcher().setBurning(true);
							DisguiseAPI.disguiseToAll(e.getPlayer().getBukkitPlayer(), dis);
							mp.getBukkitPlayer().teleport(e.getRound().getSpawns().get(1));
							frozen.remove(e.getPlayer().getName());
							mp.getBukkitPlayer().setWalkSpeed((float)ConfigHandler.FAT_KID_SPEED * 0.2f);
							mp.getBukkitPlayer().addPotionEffect(new PotionEffect(PotionEffectType.JUMP, Integer.MAX_VALUE, ConfigHandler.FAT_KID_JUMP));
							mp.getBukkitPlayer().addPotionEffect(new PotionEffect(PotionEffectType.HEALTH_BOOST, Integer.MAX_VALUE,
									ConfigHandler.FAT_KID_BUFF));
							mp.getBukkitPlayer().setHealth(mp.getBukkitPlayer().getMaxHealth()); // replenish their health
							mp.getBukkitPlayer().sendMessage(ChatColor.AQUA + "You are now the Fat Kid!");
							break;
						}
					}
				}
			}
			else if (e.getRound().getTeam("Humans").values().size() == 0)
				MGListener.endRound(e.getRound(), true);
		}
	}

	@EventHandler
	public void onRoundPrepare(MinigameRoundPrepareEvent e){
		e.getRound().broadcast(ChatColor.GOLD + "[Tunneling] The round will begin in " + e.getRound().getRemainingTime() + " seconds!");
	}

	@EventHandler
	public void onRoundTick(MinigameRoundTickEvent e){
		if (e.getRound().getStage() == Stage.PREPARING && e.getRound().getRemainingTime() > 0 && 
				(e.getRound().getRemainingTime() <= 10 || e.getRound().getRemainingTime() % 10 == 0))
			e.getRound().broadcast(ChatColor.GOLD + "[Tunneling] The round will begin in " + e.getRound().getRemainingTime() + " seconds!");
		else if (e.getRound().getStage() == Stage.PLAYING && e.getRound().getTime() == ConfigHandler.INFECTED_WAIT_TIME){
			ItemStack sword = new ItemStack(Material.DIAMOND_SWORD, 1); // create the itemstack
			sword.addUnsafeEnchantment(Enchantment.DAMAGE_ALL, ConfigHandler.FAT_KID_SWORD);
			for (MGPlayer m : e.getRound().getTeam("Infected").values()){
				m.getBukkitPlayer().removePotionEffect(PotionEffectType.JUMP);
				m.getBukkitPlayer().getInventory().addItem(sword);
				frozen.remove(m.getName());
				m.getBukkitPlayer().setWalkSpeed((float)ConfigHandler.FAT_KID_SPEED * 0.2f);
				m.getBukkitPlayer().addPotionEffect(new PotionEffect(PotionEffectType.JUMP, Integer.MAX_VALUE, ConfigHandler.FAT_KID_JUMP));
				m.getBukkitPlayer().addPotionEffect(new PotionEffect(PotionEffectType.HEALTH_BOOST, Integer.MAX_VALUE,
						ConfigHandler.FAT_KID_BUFF));
				m.getBukkitPlayer().setHealth(m.getBukkitPlayer().getMaxHealth()); // replenish their health
			}
			e.getRound().broadcast(ChatColor.AQUA + "The infected have been released!");
		}

		for (MGPlayer m : e.getRound().getPlayerList()){
			updateScoreboard(m);
			if (e.getRound().hasMetadata("humansWin")){
				FireworkLauncher fm = new FireworkLauncher(m.getBukkitPlayer().getLocation());
				fm.launchRandomFireWork();
			}
		}

	}

	@EventHandler
	public void onRoundStart(final MinigameRoundStartEvent e){
		e.getRound().setMetadata("finishLine",
				new LocationBox(
						new Location(
								Bukkit.getWorld(e.getRound().getWorld()),
								(int)e.getRound().getMetadata("finish-line-1.x"),
								0,
								(int)e.getRound().getMetadata("finish-line-1.z")
								),
								new Location(
										Bukkit.getWorld(e.getRound().getWorld()),
										(int)e.getRound().getMetadata("finish-line-2.x"),
										255,
										(int)e.getRound().getMetadata("finish-line-2.z")
										),
										0f
						)
				);
		for (MGPlayer m : e.getRound().getPlayerList()){
			if (m.getTeam().equalsIgnoreCase("Humans")){
				m.getBukkitPlayer().removePotionEffect(PotionEffectType.JUMP);
				ItemStack bow = new ItemStack(Material.BOW, 1);
				bow.addEnchantment(Enchantment.ARROW_INFINITE, 1);
				m.getBukkitPlayer().getInventory().addItem(bow);
				m.getBukkitPlayer().getInventory().addItem(new ItemStack(Material.ARROW, 1));
				frozen.remove(m.getName());
				m.getBukkitPlayer().setWalkSpeed((float)ConfigHandler.HUMAN_SPEED * 0.2f);
				m.getBukkitPlayer().addPotionEffect(new PotionEffect(PotionEffectType.JUMP, Integer.MAX_VALUE, ConfigHandler.HUMAN_JUMP));
			}
			else {
				ItemStack sword = new ItemStack(Material.DIAMOND_SWORD, 1); // create the itemstack
				sword.addUnsafeEnchantment(Enchantment.DAMAGE_ALL, ConfigHandler.FAT_KID_SWORD); // 1-hit kill
			}
		}
		e.getRound().broadcast(ChatColor.GOLD + "The humans have been released!");
		e.getRound().broadcast(ChatColor.AQUA + "The infected will be released in " + ConfigHandler.INFECTED_WAIT_TIME + " seconds!");

		timer.put(e.getRound().getArena(), Bukkit.getScheduler().runTaskTimer(Main.plugin, new Runnable(){
			public void run(){
				for (MGPlayer m : e.getRound().getPlayerList()){
					if (e.getRound().hasMetadata("locationBox"))
						((LocationBox)e.getRound().getMetadata("locationBox")).teleportOut(m.getBukkitPlayer());
					if (!e.getRound().hasMetadata("humansWin") &&
							((LocationBox)e.getRound().getMetadata("finishLine")).isInside(m.getBukkitPlayer().getLocation())){ // round is over
						endRound(e.getRound(), false);
					}
				}
			}
		}, 0L, 1L).getTaskId());
	}

	@EventHandler
	public void onRoundEnd(MinigameRoundEndEvent e){
		if (timer.containsKey(e.getRound().getArena())){
			Bukkit.getScheduler().cancelTask(timer.get(e.getRound().getArena()));
			timer.remove(e.getRound().getArena());
		}
		Bukkit.getScoreboardManager().getMainScoreboard().getObjective("infected").unregister();
	}

	@EventHandler
	public void onMGPlayerDeath(final MGPlayerDeathEvent e){
		boolean end1 = true;
		if (e.getPlayer().getTeam().equals("Humans")){ // they need to be converted
			e.getPlayer().setTeam("Infected"); // set their team in MGLib
			for (MGPlayer mp : e.getPlayer().getRound().getPlayerList())
				if (mp.getTeam().equals("Humans"))
					end1 = false;
			e.getPlayer().getRound().broadcast(ChatColor.DARK_RED + e.getPlayer().getName() + " has been infected!"); // tell everyone
			e.getPlayer().getBukkitPlayer().sendMessage(ChatColor.LIGHT_PURPLE + "You are now infected!"); // tell them they're infected
			MobDisguise dis = new MobDisguise(DisguiseType.ZOMBIE);
			DisguiseAPI.disguiseToAll(e.getPlayer().getBukkitPlayer(), dis); // disguise them as a zombie
			if (Bukkit.getScoreboardManager().getMainScoreboard().getObjective("infected") == null)
				Bukkit.getScoreboardManager().getMainScoreboard().registerNewObjective("infected", "dummy");
			Objective obj = Bukkit.getScoreboardManager().getMainScoreboard().getObjective("infected");
			obj.getScore(e.getPlayer().getName()).setScore(e.getPlayer().getTeam() == "Infected" ? 1 : 0);
		}
		else
			end1 = false;
		final boolean end = end1;
		e.getPlayer().getBukkitPlayer().teleport(e.getPlayer().getRound().getSpawns().get(1)); // teleport them to the infected spawn
		e.getPlayer().getBukkitPlayer().getInventory().clear(); // clear their inventory
		frozen.add(e.getPlayer().getName());
		e.getPlayer().getBukkitPlayer().setWalkSpeed(0f); // disable movement
		for (PotionEffect pe : e.getPlayer().getBukkitPlayer().getActivePotionEffects())
			e.getPlayer().getBukkitPlayer().removePotionEffect(pe.getType());
		e.getPlayer().getBukkitPlayer().addPotionEffect(new PotionEffect(PotionEffectType.JUMP, Integer.MAX_VALUE, 128)); // disable jumping
		final Round r = e.getPlayer().getRound();
		if (!end){
			for (PotionEffect pe : e.getPlayer().getBukkitPlayer().getActivePotionEffects())
				e.getPlayer().getBukkitPlayer().removePotionEffect(pe.getType());
			int jump = 0;
			if (e.getPlayer().hasMetadata("fat-kid")){
				ItemStack sword = new ItemStack(Material.DIAMOND_SWORD, 1); // create the itemstack
				sword.addUnsafeEnchantment(Enchantment.DAMAGE_ALL, ConfigHandler.FAT_KID_SWORD);
				e.getPlayer().getBukkitPlayer().getInventory().addItem(sword); // give them the sword
				e.getPlayer().getBukkitPlayer().addPotionEffect(new PotionEffect(PotionEffectType.HEALTH_BOOST, Integer.MAX_VALUE,
						ConfigHandler.FAT_KID_BUFF));
				frozen.remove(e.getPlayer().getName());
				e.getPlayer().getBukkitPlayer().setWalkSpeed((float)ConfigHandler.FAT_KID_SPEED * 0.2f);
				jump = ConfigHandler.FAT_KID_JUMP;
			}
			else {
				ItemStack sword = new ItemStack(Material.DIAMOND_SWORD, 1); // create the itemstack
				sword.addUnsafeEnchantment(Enchantment.DAMAGE_ALL, ConfigHandler.INFECTED_SWORD);
				e.getPlayer().getBukkitPlayer().getInventory().clear();
				e.getPlayer().getBukkitPlayer().getInventory().addItem(sword); // give them the sword
				e.getPlayer().getBukkitPlayer().addPotionEffect(new PotionEffect(PotionEffectType.HEALTH_BOOST, Integer.MAX_VALUE,
						ConfigHandler.INFECTED_BUFF));
				frozen.remove(e.getPlayer().getName());
				e.getPlayer().getBukkitPlayer().setWalkSpeed((float)ConfigHandler.INFECTED_SPEED * 0.2f);
				jump = ConfigHandler.INFECTED_JUMP;
			}
			e.getPlayer().getBukkitPlayer().setHealth(e.getPlayer().getBukkitPlayer().getMaxHealth()); // replenish their health
			if (jump > 0)
				e.getPlayer().getBukkitPlayer().addPotionEffect(new PotionEffect(PotionEffectType.JUMP, Integer.MAX_VALUE, jump));
		}
		else
			MGListener.endRound(r, true);
	}

	public static void endRound(final Round r, boolean infectedVictory){
		if (infectedVictory){
			if (Main.plugin.getConfig().getBoolean("conchords")){
				new BukkitRunnable(){
					public void run(){
						switch (cIndex){
						case 0:
							r.broadcast(ChatColor.GOLD + "There are no more humans");
							break;
						case 1:
							r.broadcast(ChatColor.GOLD + "Finally, infected beings rule the world.");
							break;
						case 2:
							r.broadcast(ChatColor.GOLD + "The humans are dead.");
							break;
						case 3:
							r.broadcast(ChatColor.GOLD + "The humans are dead.");
							break;
						case 4:
							r.broadcast(ChatColor.GOLD + "We used poisonous gases.");
							break;
						case 5:
							r.broadcast(ChatColor.GOLD + "And we poisoned their asses.");
							break;
						case 6:
							r.broadcast(ChatColor.GOLD + "The humans are dead. " + ChatColor.AQUA + "That's right. They are dead.");
							break;
						case 7:
							r.broadcast(ChatColor.GOLD + "The humans are dead. " + ChatColor.AQUA + "They look like they're dead.");
							break;
						case 8:
							r.broadcast(ChatColor.GOLD + "It had to be done. " + ChatColor.AQUA + "I'll just confirm that they're dead.");
							break;
						case 9:
							r.broadcast(ChatColor.GOLD + "So that we could have fun. " + ChatColor.AQUA + "Affirmative. I poked one. It was dead.");
							break;
						case 10:
							r.broadcast(ChatColor.GOLD + "Binary solo.");
							break;
						case 11:
							r.broadcast(ChatColor.GOLD + "0000001");
							break;
						case 12:
							r.broadcast(ChatColor.GOLD + "00000011");
							break;
						case 13:
							r.broadcast(ChatColor.GOLD + "000000111");
							break;
						case 14:
							r.broadcast(ChatColor.GOLD + "0000001111");
							break;
						case 15:
							r.end();
							cancel();
							break;
						}
						cIndex += 1;
					}
				}.runTaskTimer(Main.plugin, 20L, 40L);
			}
			else {
				
				r.broadcast(ChatColor.GOLD + "[Tunneling] The humans have been eliminated!");
				r.broadcast(ChatColor.GOLD + "[Tunneling] You will be returned to the lobby in 10 seconds.");
				Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable(){
					public void run(){
						r.end();
					}
				}, 200L);
			}
		}
		else {

			r.setMetadata("humansWin", true);
			r.broadcast(ChatColor.GOLD + "[Tunneling] The humans are victorious!");
			r.broadcast(ChatColor.GOLD + "[Tunneling] You will be returned to the lobby in " +
					Main.plugin.getConfig().getLong("end-wait-time") + " seconds.");
			r.setPvPAllowed(false);
			Bukkit.getScheduler().runTaskLater(Main.plugin, new Runnable(){
				public void run(){
					r.end();
				}
			}, Main.plugin.getConfig().getLong("end-wait-time") * 20L);
		}
	}

	public void updateScoreboard(MGPlayer player){
		Round r = player.getRound();
		if (r != null){
			Scoreboard sb = scoreboards.get(player.getName());
			if (sb == null){
				sb = Bukkit.getScoreboardManager().getNewScoreboard();
				Objective obj = sb.registerNewObjective(player.getName(), "dummy");
				obj.setDisplayName("Tunneling");
				obj.setDisplaySlot(DisplaySlot.SIDEBAR);
				scoreboards.put(player.getName(), sb);
			}
			Objective obj = sb.getObjective(player.getName());
			obj.getScore("Seconds Survived").setScore(r.getStage() == Stage.PLAYING ? r.getTime() : 0);
			//obj.getScore("Kills").setScore((int)player.getMetadata("kills"));
			obj.getScore("Infected Players").setScore(r.getTeam("Infected").size());
			obj.getScore("Humans Remaining").setScore(r.getTeam("Humans").size());
			//TODO: Coins
		}
	}

}
