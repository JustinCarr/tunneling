package net.amigocraft.tunneling;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import java.util.logging.Logger;

import me.libraryaddict.disguise.DisguiseAPI;
import net.amigocraft.mglib.api.ConfigManager;
import net.amigocraft.mglib.api.MGPlayer;
import net.amigocraft.mglib.api.Minigame;
import net.amigocraft.mglib.api.Round;
import net.amigocraft.mglib.exception.PlayerNotPresentException;
import net.amigocraft.mglib.exception.PlayerOfflineException;
import net.amigocraft.tunneling.listeners.BlockListener;
import net.amigocraft.tunneling.listeners.MGListener;
import net.amigocraft.tunneling.listeners.PlayerListener;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

public class Main extends JavaPlugin {

	public static Main plugin;
	public static Logger log;
	public static Minigame mg;

	public static HashMap<String, Integer> wizardStages = new HashMap<String, Integer>();
	public static HashMap<String, String> wizardArenas = new HashMap<String, String>();
	private static ArrayList<UUID> toclearbalance = new ArrayList<UUID>();
	
	public static final String notice_prefix = ChatColor.DARK_RED + "" + ChatColor.BOLD + "[" + ChatColor.GREEN + "" + ChatColor.BOLD + "##" + ChatColor.DARK_RED + "" + ChatColor.BOLD + "]" + ChatColor.RED + " ";
	public static final String red_color = ChatColor.RED + "";
	public static final String green_color = ChatColor.GREEN + "" + ChatColor.BOLD;
	public static final int max_coin_value = Integer.MAX_VALUE;
	
	public static ConfigManager cm;

	public static List<String> violations = new ArrayList<String>();

	public void onEnable(){

		plugin = this;
		log = getLogger();
		mg = Minigame.registerPlugin(this);
		saveDefaultConfig();
		ConfigHandler.initialize();

		cm = mg.getConfigManager();
		cm.setMinPlayers(getConfig().getInt("min-players"));
		cm.setMaxPlayers(getConfig().getInt("max-players"));
		cm.setOverrideDeathEvent(true);
		cm.setKitsAllowed(false);
		cm.setAllowJoinRoundInProgress(false);
		cm.setBlockBreakAllowed(true);
		cm.setBlockPhyiscsAllowed(true);
		cm.setDefaultPreparationTime(getConfig().getInt("preparation-time"));
		cm.setDefaultPlayingTime(0);
		cm.setRandomSpawning(false);
		cm.setTeleportationAllowed(false);
		cm.setTeamDamageAllowed(false);
		cm.setPvPAllowed(true);
		cm.setAllowJoinRoundWhilePreparing(true);
		cm.setSpectateOnJoin(false);

		if (getConfig().isSet("spawn")){
			try {
				cm.setDefaultExitLocation(new Location(Bukkit.getWorld(getConfig().getString("spawn.w")),
						getConfig().getInt("spawn.x"),
						getConfig().getInt("spawn.y"),
						getConfig().getInt("spawn.z")));
			}
			catch (Exception ex){}
		}

		getServer().getPluginManager().registerEvents(new BlockListener(), this);
		getServer().getPluginManager().registerEvents(new MGListener(), this);
		getServer().getPluginManager().registerEvents(new PlayerListener(), this);

		
		SQLManager.UpdateUUIDsWithCurrentNames();
		
		log.info(this + " has been enabled!");
	}

	public void onDisable(){
		for (Round r : mg.getRoundList())
			for (MGPlayer m : r.getPlayerList()){
				m.getBukkitPlayer().setScoreboard(Bukkit.getScoreboardManager().getNewScoreboard());
				m.getBukkitPlayer().setWalkSpeed(0.2f);
				DisguiseAPI.undisguiseToAll(m.getBukkitPlayer());
			}
		log.info(this + " has been disabled!");
		mg = null;
		log = null;
		plugin = null;
		toclearbalance.clear();
		
		SQLManager.destroyConnection();
		
	}

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args){
		if (label.equalsIgnoreCase("tunneling")){
			if (args.length > 0){
				if (sender instanceof Player){
					if (args[0].equalsIgnoreCase("create")){
						if (sender.hasPermission("tunneling.create")){
							sender.sendMessage(ChatColor.DARK_AQUA + "Welcome to the Tunneling arena creation wizard! " +
									"To start, please type the name of the arena you would like to create.");
							sender.sendMessage(ChatColor.DARK_AQUA + "Note: you may type \"quit\" to exit the wizard at any time, "
									+ "or \"back\" to return to the previous step. "
									+ "Commands will be ignored by the wizard and processed normally.");
							wizardStages.put(sender.getName(), 0);
						}
						else
							sender.sendMessage(ChatColor.RED + "[Tunneling] You do not have permission to use this command!");
					}
					else if (args[0].equalsIgnoreCase("quit")){
						try {
							if (mg.isPlayer(sender.getName())){
								mg.getMGPlayer(sender.getName()).removeFromRound();
								sender.sendMessage(ChatColor.GOLD + "[Tunneling] You have been removed from the arena.");
							}
							else
								sender.sendMessage(ChatColor.RED + "[Tunneling] You are not in a round!");
						}
						catch (PlayerNotPresentException ex){
							sender.sendMessage(ChatColor.RED + "[Tunneling] You are not in a round!");
						}
						catch (PlayerOfflineException ex){}
					}
					else if (args[0].equalsIgnoreCase("setspawn")){
						getConfig().set("spawn.w", ((Player)sender).getWorld().getName());
						getConfig().set("spawn.x", ((Player)sender).getLocation().getBlockX());
						getConfig().set("spawn.y", ((Player)sender).getLocation().getBlockY());
						getConfig().set("spawn.z", ((Player)sender).getLocation().getBlockZ());
						saveConfig();
						mg.getConfigManager().setDefaultExitLocation(((Player)sender).getLocation().getBlock().getLocation());
						sender.sendMessage(ChatColor.GOLD + "[Tunneling] The default exit location has been set.");
					}
					else
						sender.sendMessage(ChatColor.RED + "[Tunneling] Invalid arguments! Usage: /tunneling [args]");
				}
				else
					sender.sendMessage(ChatColor.RED + "[Tunneling] You must be a player to use this command!");
			}
			else
				sender.sendMessage(ChatColor.RED + "[Tunneling] Too few arguments! Usage: /tunneling [args]");
			return true;
		
		} 
		
		/* START ADDITIONS */ 
		
		else if (label.equalsIgnoreCase("coins")){
			
			if (args.length == 0 || (args[0].equals("balance"))){
				
				if (args.length > 1){
					
					if (sender.hasPermission("tunneling.coinsadmin")){
						
						Player p = null;
						
						for (Player search : getServer().getOnlinePlayers()){
							if (search.getName().equals(args[1])){
								p = search;
								break;
							}
						}
						
						int coins = 0;
						if (p != null){
							coins = SQLManager.getCoins(p);
						} else {
							coins = SQLManager.getCoins(args[1]);							
						}
						
						sender.sendMessage(notice_prefix + "Player " + green_color + args[1] + red_color + " has " + green_color + coins + red_color + " coin(s)!");

						
					} else {
						sender.sendMessage(red_color + "You don't have permission for this!");
					}
					return true;
					
				}
				
				if (!(sender instanceof Player)){
					sender.sendMessage(ChatColor.RED + "Oops. This command can only be used by players!");
					return true;
				}
				int coins = SQLManager.getCoins((Player) sender);
				if (coins <= 0){
					sender.sendMessage(notice_prefix + "You don't have any coins right now!");
				} else if (coins > max_coin_value){
					sender.sendMessage(notice_prefix + "You have " + green_color + max_coin_value + " coins!");
				} else {
					sender.sendMessage(notice_prefix + "You have " + green_color + coins + " coins!");
				}
			
			} else if (args[0].equalsIgnoreCase("resetbalance")){
				
				if (args.length > 1){
					if (sender.hasPermission("tunneling.coinsadmin")){
						
						Player p = null;
						
						for (Player search : getServer().getOnlinePlayers()){
							if (search.getName().equals(args[1])){
								p = search;
								break;
							}
						}
						
						if (p != null){
							sender.sendMessage(notice_prefix + "Player " + green_color + args[1] + red_color + "'s balance has been reset!");
							SQLManager.removeAllCoins(p);
						} else {
							
							sender.sendMessage(notice_prefix + "Player " + green_color + args[1] + red_color + "'s balance has been reset!");
							SQLManager.removeAllCoins(args[1]);
							
						}
						
					} else {
						sender.sendMessage(red_color + "You don't have permission for this!");
					}
				} else {
					
					if (!(sender instanceof Player)){
						sender.sendMessage(ChatColor.RED + "Oops. This command can only be used by players!");
						return true;
					}
					Player p = (Player) sender;
					final UUID playerid = p.getUniqueId();
					
					if (toclearbalance.contains(playerid)){
						toclearbalance.remove(playerid);
						p.sendMessage(notice_prefix + "All of your coins have been removed!");
						SQLManager.removeAllCoins(p);
					} else {
						p.sendMessage(notice_prefix + "Are you sure you want to remove ALL your coins?");
						p.sendMessage(notice_prefix + "If so, repeat the command. You have " + green_color + "10" + red_color + " seconds.");
						toclearbalance.add(playerid);
						
						getServer().getScheduler().scheduleSyncDelayedTask(this, new BukkitRunnable(){
							public void run(){
								toclearbalance.remove(playerid);
							}
						}, 200L);
						
					}
					
				}
				
			} else if (args[0].equalsIgnoreCase("remove") || args[0].equalsIgnoreCase("take")){
				
				if (args.length < 3){
					
					sender.sendMessage(red_color + "Oops. Incorrect Usage. Try: " + ChatColor.DARK_RED + "" + ChatColor.BOLD + "/coins remove PLAYER AMOUNT");
					return true;
					
				}
				int coins = 0;
				try {
					coins = Integer.parseInt(args[2]);
				} catch (NumberFormatException e){
					sender.sendMessage(red_color + "Oops. The AMOUNT of coins argument must be an Integer.");
					return true;
				}
				
				if (coins <= 0){
					sender.sendMessage(red_color + "Oops. The AMOUNT of coins must be greater than 0.");
					return true;
				}
				
				Player toremove = null;
				for (Player test : getServer().getOnlinePlayers()){
					if (test.getName().equals(args[1])){
						toremove = test;
						break;
					}
				}
				if (toremove != null){
					SQLManager.removeCoins(toremove, coins);
	    			sender.sendMessage(notice_prefix + green_color + coins + red_color + " coins have been taken from " + "\"" + toremove.getName() + "\"!");
				} else {
					SQLManager.removeCoins(args[1], coins);
	    			sender.sendMessage(notice_prefix + green_color + coins + red_color + " coins have been taken from " + "\"" + args[1] + "\"!");
				}
				
			} else if (args[0].equalsIgnoreCase("add") || args[0].equalsIgnoreCase("give") || args[0].equalsIgnoreCase("grant")){
				
				if (args.length < 3){
					
					sender.sendMessage(red_color + "Oops. Incorrect Usage. Try: " + ChatColor.DARK_RED + "" + ChatColor.BOLD + "/coins add PLAYER AMOUNT");
					return true;
					
				}
				int coins = 0;
				try {
					coins = Integer.parseInt(args[2]);
				} catch (NumberFormatException e){
					sender.sendMessage(red_color + "Oops. The AMOUNT of coins argument must be an Integer.");
					return true;
				}
				
				if (coins <= 0){
					sender.sendMessage(red_color + "Oops. The AMOUNT of coins must be greater than 0.");
					return true;
				}
				
				Player toadd = null;
				for (Player test : getServer().getOnlinePlayers()){
					if (test.getName().equals(args[1])){
						toadd = test;
						break;
					}
				}
				if (toadd != null){
					SQLManager.addCoins(toadd, coins);
	    			sender.sendMessage(notice_prefix + "\"" + toadd.getName() + "\" has been given " + green_color + coins + Main.red_color + " coins!");
				} else {
					SQLManager.addCoins(args[1], coins, sender);
					sender.sendMessage(notice_prefix + "Attempting to add the coins, please wait...");
				}
				
			} else if (args[0].equalsIgnoreCase("help") || args[0].equals("?")) {
				
				boolean hasperm = sender.hasPermission("tunneling.coinsadmin");
				
				String g = ChatColor.GOLD + "";
				String w = ChatColor.WHITE + "";
				
				sender.sendMessage(ChatColor.AQUA + "==================================================");
				sender.sendMessage(ChatColor.GRAY + "Here's how to use the '/coins' command...");
				sender.sendMessage(g + "/coins balance " + w + "See how many coins you have");
				if (hasperm){
					sender.sendMessage(g + "/coins balance PLAYER " + w + "See how many coins PLAYER has");
				}
				sender.sendMessage(g + "/coins clearbalance " + w + "CLEARS ALL OF YOUR COINS!");
				if (hasperm){
					sender.sendMessage(g + "/coins clearbalance PLAYER " + w + "Clears all of PLAYER's coins");
					sender.sendMessage(g + "/coins add PLAYER AMOUNT " + w + "Gives AMOUNT of coins to PLAYER");
					sender.sendMessage(g + "/coins remove PLAYER AMOUNT " + w + "Takes AMOUNT of coins from PLAYER");
				}
				sender.sendMessage(ChatColor.AQUA + "==================================================");

				
			} else {
				sender.sendMessage(red_color + "Oops. Incorrect Usage. For help: " + ChatColor.RED + "" + ChatColor.BOLD + "/coins help");
			}
			return true;
			
		}
		/* END ADDITIONS */
		
		return false;
	}

	/*public static void setCheckMovement(Player player, boolean check){
		try {
			String version = "";
			String[] array = Bukkit.getServer().getClass().getPackage().getName().replace(".", ",").split(",");
			if (array.length == 4)
				version = array[3] + ".";
			Object craftPlayer = Class.forName("org.bukkit.craftbukkit." + version + "entity.CraftPlayer").cast(player);
			Method m = craftPlayer.getClass().getMethod("getHandle");
			Object handle = m.invoke(craftPlayer);
			final Object connection = handle.getClass().getDeclaredField("playerConnection").get(handle);
			connection.getClass().getDeclaredField("checkMovement").set(connection, check);
			System.out.println("flag set - " + connection.getClass().getDeclaredField("checkMovement").get(connection));
			Bukkit.getScheduler().runTask(plugin, new Runnable(){
				public void run(){
					try {
						System.out.println("flag is now " + connection.getClass().getDeclaredField("checkMovement").get(connection));
					} catch (IllegalArgumentException | IllegalAccessException
							| NoSuchFieldException | SecurityException e) {
						e.printStackTrace();
					}
				}
			});
		}
		catch (IllegalArgumentException | IllegalAccessException | NoSuchFieldException
				| SecurityException | NoSuchMethodException | InvocationTargetException | ClassNotFoundException ex) {
			ex.printStackTrace();
		}
	}*/

}
