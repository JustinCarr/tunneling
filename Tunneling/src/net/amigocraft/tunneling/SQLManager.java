package net.amigocraft.tunneling;

import java.nio.ByteBuffer;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;



public class SQLManager {

	private static Connection cnc = null;
	private static final String prefix = "[Tunneling] ";
	public static String host = "jdbc:mysql://localhost:3306/minecraft";
	public static String username = "root";
	public static String password = "password";
	
    private final static String PLAYERS_TABLE = "CREATE TABLE `playerCoins` (" + 
            "`playeruuid` blob(16)," + 
    		"`playername` varchar(100) NOT NULL," +
            "`coins` INTEGER NOT NULL DEFAULT '0',";
	
    
    public static Connection connectToDB() {
        if (cnc == null){
            cnc = initialize();
        }
        try {
            if (!cnc.isValid(10))
                cnc = initialize();
        } catch (SQLException ex) {
            Main.log.severe(prefix + "Error trying to check the SQL status. INFO:");
            Main.log.severe(prefix + "  " + ex.getMessage());
        }
        
        return cnc;
    }
    
    
    
    public static void destroyConnection() {
        if (cnc != null) {
            try {
                    if (cnc.isValid(10)) {
                        cnc.close();
                    }
                    cnc = null;
                
            } catch (SQLException ex) {
                Main.log.severe("[Tunneling] Error trying to close the connection. INFO:");
                Main.log.severe("[Tunneling] " + ex.getMessage());
            }
        }
    }
    
    //BELOW IS UTILITY
    private static Connection initialize() {
    	try {
    		Class.forName("com.mysql.jdbc.Driver");
            cnc = DriverManager.getConnection(host, username, password);
            cnc.setAutoCommit(false);
            
        } catch (SQLException ex) {
            Main.log.severe(prefix + "Error on initiate. INFO:");
            Main.log.severe(prefix + "  " + ex.getMessage());
        } catch (ClassNotFoundException ex) {
            Main.log.severe(prefix + "Error on initiate. You need the SQLite/MySQL library. INFO:");
            Main.log.severe(prefix + "  " + ex.getMessage());
        }

        createTable();

        return cnc;
    }
    
    private static void createTable(){
    	Statement st = null;
        try {
            Connection conn = connectToDB();
            st = conn.createStatement();

            if (!tableExists()) {
                st.executeUpdate(PLAYERS_TABLE);
                conn.commit();
            }
                        
        } catch (SQLException ex) {
            Main.log.severe(prefix + "Error creating SQL table. INFO:");
            ex.printStackTrace();
        } finally {
            try {
                if (st != null) {
                    st.close();
                }
            } catch (SQLException ex) {
                Main.log.severe(prefix + "Error, could not create SQL table. INFO:");
                Main.log.severe(prefix + "  " + ex.getMessage());
            }
        }
    }
    
    private static boolean tableExists() {
        ResultSet rs = null;
        try {
            Connection conn = connectToDB();

            DatabaseMetaData dbm = conn.getMetaData();
            rs = dbm.getTables(null, null, "playerCoins", null);
            if (!rs.next())
                return false;
            return true;
        } catch (SQLException ex) {
            Main.log.severe(prefix + "Error on SQL table check. INFO:");
            Main.log.severe(prefix + "  " + ex.getMessage());
            return false;
        } finally {
            try {
                if (rs != null)
                    rs.close();
            } catch (SQLException ex) {
                Main.log.severe(prefix + "Error on SQL table check. INFO:");
                Main.log.severe(prefix + "  " + ex.getMessage());
            }
        }
    }
    
    //THIS IS LESS PREFERABLE... USE THE METHOD WITH THE PLAYER ARG IF POSSIBLE
    public static void removeAllCoins(String p){
    	
    	if (p != null){
    		PreparedStatement pst = null;
    		
    		try {
    			
    			Connection conn = connectToDB();
        		
        		pst = conn.prepareStatement("DELETE FROM playerCoins WHERE playername = ?");
        		pst.setString(1, p);
        		
        		pst.executeUpdate();
        		conn.commit();
        		pst.close();
    		
    		} catch (SQLException ex){
                Main.log.severe(prefix + "Error on SQL delete all coins. INFO:");
                Main.log.severe(prefix + "  " + ex.getMessage());

    		} finally {
    			
    			try {
    				
        			if (pst != null){
        				pst.close();
        			}
    			} catch (SQLException ex){
                    Main.log.severe(prefix + "Error on SQL close of deleting all coins. INFO:");
                    Main.log.severe(prefix + "  " + ex.getMessage());
    			}	
    		}
    	}
    	
    }
    
    public static void removeAllCoins(Player p){
    	
    	if (p != null){
    		PreparedStatement pst = null;
    		
    		try {
    			
    			Connection conn = connectToDB();
        		
        		byte[] bytes = toBytes(p.getUniqueId());
        		pst = conn.prepareStatement("DELETE FROM playerCoins WHERE playeruuid = ?");
        		pst.setBytes(1, bytes);
        		
        		pst.executeUpdate();
        		conn.commit();
        		pst.close();
    		
    		} catch (SQLException ex){
                Main.log.severe(prefix + "Error on SQL delete all coins. INFO:");
                Main.log.severe(prefix + "  " + ex.getMessage());

    		} finally {
    			
    			try {
    				
        			if (pst != null){
        				pst.close();
        			}
    			} catch (SQLException ex){
                    Main.log.severe(prefix + "Error on SQL close of deleting all coins. INFO:");
                    Main.log.severe(prefix + "  " + ex.getMessage());
    			}	
    		}
    	}
    }
    
    public static void addCoinsToPlayers(Map<Player[], Integer> pmatrix){
    	
    	if (pmatrix != null && !pmatrix.isEmpty()){
    		
    		PreparedStatement prepselect = null;
    		PreparedStatement prepinsert = null;
    		PreparedStatement prepupdate = null;
    		ResultSet set = null;
    		
    		try {
    			
    			Connection conn = connectToDB();
    			
    			prepselect = conn.prepareStatement("SELECT coins FROM playerCoins WHERE playeruuid = ?", ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_UPDATABLE);
    			prepinsert = conn.prepareStatement("INSERT INTO playerCoins (playeruuid, playername, coins) VALUES (?,?,?)", ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_UPDATABLE);
    			prepupdate = conn.prepareStatement("UPDATE playerCoins SET coins = ? WHERE playeruuid = ?", ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_UPDATABLE);
    			
    			for (Entry<Player[], Integer> en : pmatrix.entrySet()){
    				
    				if (en.getValue() > 0){
    					
    					for (Player toexecute : en.getKey()){
        					
        					byte[] bytes = toBytes(toexecute.getUniqueId());
        					prepselect.setBytes(1, bytes);
        					set = prepselect.executeQuery();
        					
        					if (set.next()){
        						
        						int i = set.getInt("coins");
        						
        						if (i < 0){ i = 0;}
        						
        						prepupdate.setInt(1, i + en.getValue());
        						prepupdate.setBytes(2, bytes);
        						prepupdate.addBatch();

        					} else {
        						
        						prepinsert.setBytes(1, bytes);
        						prepinsert.setString(2, toexecute.getName());
        						prepinsert.setInt(3, en.getValue());
        						prepinsert.addBatch();
        					
        					}
        					
        					set.close();
        					
        				}
    				}
    			}
    			
    			prepinsert.executeBatch();
    			prepupdate.executeBatch();
    			conn.commit();
    		
    		} catch (SQLException ex){
                Main.log.severe(prefix + "Error on SQL adding coins to multiple players. INFO:");
                Main.log.severe(prefix + "  " + ex.getMessage());
    		} finally {
    			
    			try {
    				if (prepselect != null){
        				prepselect.close();
        			}
        			if (prepinsert != null){
        				prepinsert.close();
        			}
        			if (prepupdate != null){
        				prepupdate.close();
        			}
        			if (set != null){
        				set.close();
        			}
    			} catch (SQLException ex){
                    Main.log.severe(prefix + "Error on SQL close of adding coins to multiple players. INFO:");
                    Main.log.severe(prefix + "  " + ex.getMessage());
    			}
    		}
    		
    	}
    	
    }
    
	//THIS IS LESS PREFERABLE... USE THE METHOD WITH THE PLAYER ARG IF POSSIBLE
    public static void addCoins(final String p , int toadd, final CommandSender sender){
    	
    	if (toadd > 0 && p != null){
    		
    		PreparedStatement pst = null;
    		ResultSet set = null;
    		
    		try {
    			
    			Connection conn = connectToDB();
        		
    			
        		pst = conn.prepareStatement("SELECT coins FROM playerCoins WHERE playername = ?");
        		pst.setString(1, p);
        		
        		set = pst.executeQuery();
        		
        		
        		if (set.next()){
        			int coins = set.getInt("coins");
        			
        			if (coins < 0){
            			coins = 0;
            		}
        			
        			if (Main.max_coin_value - toadd >= coins){
        				
        				pst = conn.prepareStatement("UPDATE playerCoins SET coins = ? WHERE playername = ?");
        				pst.setInt(1, coins + toadd);
        				pst.setString(2, p);
        				pst.executeUpdate();
        				conn.commit();
        				
        			} else {
        				pst = conn.prepareStatement("UPDATE playerCoins SET coins = ? WHERE playername = ?");
        				pst.setInt(1, Main.max_coin_value);
        				pst.setString(2, p);
        				pst.executeUpdate();
        				conn.commit();
        			}
        			
        		} else {
        			
        			Bukkit.getServer().getScheduler().runTaskAsynchronously(Main.plugin, new BukkitRunnable(){
        				public void run(){
        					
        					try {
            					UUID uuid = UUIDManager.getUUIDOf(p);
            					
            					if (uuid != null){
            						insertTheCoins(sender, uuid, p, 1);
            					} else {
        							sender.sendMessage(Main.notice_prefix + "Mojang doesn't recognize the name '" + p + "'.");
        							sender.sendMessage(Main.notice_prefix + "No coins have been added.");
            					}
        					} catch (Exception e){
        						if (sender != null){
        							sender.sendMessage(Main.notice_prefix + "Mojang doesn't recognize the name '" + p + "'.");
        							sender.sendMessage(Main.notice_prefix + "No coins have been added.");
        						}
        					}
        					
        					
        				}
        			});
        			
        		}
    		
    		} catch (SQLException ex){
                Main.log.severe(prefix + "Error on SQL add coins. INFO:");
                Main.log.severe(prefix + "  " + ex.getMessage());
    		} finally {
    			
    			try {
    				if (set != null){
        				set.close();
        			}
        			if (pst != null){
        				pst.close();
        			}
    			} catch (SQLException ex){
                    Main.log.severe(prefix + "Error on SQL close of adding coins. INFO:");
                    Main.log.severe(prefix + "  " + ex.getMessage());
    			}
    		}
    	}
    }
    
    private static void insertTheCoins(CommandSender sender, UUID uuid, String p, int coins){
    	
    	PreparedStatement pst = null;
    	try {
    		
    		Connection conn = connectToDB();
    		
    		pst = conn.prepareStatement("INSERT INTO playerCoins (playeruuid, playername, coins) VALUES (?,?,?)");
    		pst.setBytes(1, toBytes(uuid));
    		pst.setString(2, p);
    		pst.setInt(3, coins);
    		pst.executeUpdate();
    		conn.commit();
    		pst.close();
    		
    		if (sender != null){
    			sender.sendMessage(Main.notice_prefix + "\"" + p + "\" has been given " + Main.green_color + coins + Main.red_color + " coins!");
    		}
    		
    	} catch (SQLException ex){
            Main.log.severe(prefix + "Error on SQL add coins. INFO:");
            Main.log.severe(prefix + "  " + ex.getMessage());

    	} finally {
    		
    		try {
    			if (pst != null){
    				pst.close();
    			}
    		} catch (SQLException ex){
                Main.log.severe(prefix + "Error on SQL close of adding coins. INFO:");
                Main.log.severe(prefix + "  " + ex.getMessage());

    		}
    		
    	}
    	
    	
    	
    }
    
    public static void addCoins(Player p , int toadd){
    	
    	if (toadd > 0 && p != null){
    		
    		PreparedStatement pst = null;
    		ResultSet set = null;
    		
    		try {
    			
    			Connection conn = connectToDB();
        		
    			
        		pst = conn.prepareStatement("SELECT coins FROM playerCoins WHERE playeruuid = ?");
        		byte[] bytes = toBytes(p.getUniqueId());
        		pst.setBytes(1, bytes);
        		
        		set = pst.executeQuery();
        		
        		
        		if (set.next()){
        			int coins = set.getInt("coins");
        			
        			if (coins < 0){
            			coins = 0;
            		}
        			
        			if (Main.max_coin_value - toadd >= coins){
        				
        				pst = conn.prepareStatement("UPDATE playerCoins SET coins = ?, playername = ? WHERE playeruuid = ?");
        				pst.setInt(1, coins + toadd);
        				pst.setString(2, p.getName());
        				pst.setBytes(3, bytes);
        				pst.executeUpdate();
        				conn.commit();
        				
        			} else {
        				pst = conn.prepareStatement("UPDATE playerCoins SET coins = ?, playername = ? WHERE playeruuid = ?");
        				pst.setInt(1, Main.max_coin_value);
        				pst.setString(2, p.getName());
        				pst.setBytes(3, bytes);
        				pst.executeUpdate();
        				conn.commit();
        			}
        			
        		} else {
        			
        			pst = conn.prepareStatement("INSERT INTO playerCoins (playeruuid, playername, coins) VALUES (?,?,?)");
        			pst.setBytes(1, bytes);
        			pst.setString(2, p.getName());
        			pst.setInt(3, toadd);
        			pst.executeUpdate();
        			conn.commit();
        			
        		}
    		
    		} catch (SQLException ex){
                Main.log.severe(prefix + "Error on SQL add coins. INFO:");
                Main.log.severe(prefix + "  " + ex.getMessage());
    		} finally {
    			
    			try {
    				if (set != null){
        				set.close();
        			}
        			if (pst != null){
        				pst.close();
        			}
    			} catch (SQLException ex){
                    Main.log.severe(prefix + "Error on SQL close of adding coins. INFO:");
                    Main.log.severe(prefix + "  " + ex.getMessage());
    			}
    		}
    	}
    }
    
    public static void removeCoins(Player p , int toremove){
    	
    	if (toremove > 0 && p != null){
    		
    		PreparedStatement pst = null;
    		ResultSet set = null;
    		
    		try {
    			
    			Connection conn = connectToDB();
        		
    			
        		pst = conn.prepareStatement("SELECT coins FROM playerCoins WHERE playeruuid = ?");
        		byte[] bytes = toBytes(p.getUniqueId());
        		pst.setBytes(1, bytes);
        		
        		set = pst.executeQuery();
        		
        		
        		if (set.next()){
        			int coins = set.getInt("coins");
        			
        			
        			if (coins < 0 || ((coins - toremove) < 0)){
            			coins = 0;
                		pst = conn.prepareStatement("DELETE FROM playerCoins WHERE playeruuid = ?");
                		pst.setBytes(1, bytes);
            			return;
            		} else {
            			
            			pst = conn.prepareStatement("UPDATE playerCoins SET coins = ?, playername = ? WHERE playeruuid = ?");
        				pst.setInt(1, coins - toremove);
        				pst.setString(2, p.getName());
        				pst.setBytes(3, bytes);
        				pst.executeUpdate();
        				conn.commit();
            			
            		}
        		}
    		
    		} catch (SQLException ex){
                Main.log.severe(prefix + "Error on SQL remove coins. INFO:");
                Main.log.severe(prefix + "  " + ex.getMessage());
    		} finally {
    			
    			try {
    				if (set != null){
        				set.close();
        			}
        			if (pst != null){
        				pst.close();
        			}
    			} catch (SQLException ex){
                    Main.log.severe(prefix + "Error on SQL close of removing coins. INFO:");
                    Main.log.severe(prefix + "  " + ex.getMessage());
    			}
    		}
    	}
    }
    
	//THIS IS LESS PREFERABLE... USE THE METHOD WITH THE PLAYER ARG IF POSSIBLE
    public static void removeCoins(String p, int toremove){
    	
    	if (toremove > 0 && p != null){
    		
    		PreparedStatement pst = null;
    		ResultSet set = null;
    		
    		try {
    			
    			Connection conn = connectToDB();
        		
    			
        		pst = conn.prepareStatement("SELECT coins FROM playerCoins WHERE playername = ?");
        		pst.setString(1, p);
        		
        		set = pst.executeQuery();
        		
        		
        		if (set.next()){
        			int coins = set.getInt("coins");
        			
        			
        			if (coins < 0 || ((coins - toremove) < 0)){
            			coins = 0;
                		pst = conn.prepareStatement("DELETE FROM playerCoins WHERE playername = ?");
                		pst.setString(1, p);
            			return;
            		} else {
            			
            			pst = conn.prepareStatement("UPDATE playerCoins SET coins = ? WHERE playername = ?");
        				pst.setInt(1, coins - toremove);
        				pst.setString(2, p);
        				pst.executeUpdate();
        				conn.commit();
            			
            		}
        		}
    		
    		} catch (SQLException ex){
                Main.log.severe(prefix + "Error on SQL remove coins. INFO:");
                Main.log.severe(prefix + "  " + ex.getMessage());
    		} finally {
    			
    			try {
    				if (set != null){
        				set.close();
        			}
        			if (pst != null){
        				pst.close();
        			}
    			} catch (SQLException ex){
                    Main.log.severe(prefix + "Error on SQL close of removing coins. INFO:");
                    Main.log.severe(prefix + "  " + ex.getMessage());
    			}
    		}
    	}
    }
    
    //THIS IS LESS PREFERABLE... USE THE METHOD WITH THE PLAYER ARG IF POSSIBLE
    public static int getCoins(String p){
    	
    	if (p != null){
    		
    		PreparedStatement pst = null;
    		ResultSet set = null;
    		
    		try {
    			
    			Connection conn = connectToDB();
        		
        		pst = conn.prepareStatement("SELECT coins FROM playerCoins WHERE playername = ?");
        		pst.setString(1, p);
        		
        		set = pst.executeQuery();

        		int coins = 0;
        		if (set.next()){
        			coins = set.getInt("coins");
        		}
        		
        		if (coins < 0){
        			coins = 0;
        			pst.close();

        			pst = conn.prepareStatement("DELETE FROM playerCoins WHERE playername = ?");
        			pst.setString(1, p);
        			pst.executeUpdate();
        			conn.commit();
        			pst.close();
        		}
        		
        		return coins;
    		
    		} catch (SQLException ex){
                Main.log.severe(prefix + "Error on SQL grab coins. INFO:");
                Main.log.severe(prefix + "  " + ex.getMessage());
                return 0;
    		} finally {
    			
    			try {
    				if (set != null){
        				set.close();
        			}
        			if (pst != null){
        				pst.close();
        			}
    			} catch (SQLException ex){
                    Main.log.severe(prefix + "Error on SQL close of grabbing coins. INFO:");
                    Main.log.severe(prefix + "  " + ex.getMessage());
    			}	
    		}
    	} else {
    		return 0;
    	}
    	
    }
    
    public static int getCoins(Player p){
    	
    	if (p != null){
    		
    		PreparedStatement pst = null;
    		ResultSet set = null;
    		
    		try {
    			
    			Connection conn = connectToDB();
        		
        		byte[] bytes = toBytes(p.getUniqueId());
        		pst = conn.prepareStatement("SELECT coins FROM playerCoins WHERE playeruuid = ?");
        		pst.setBytes(1, bytes);
        		
        		set = pst.executeQuery();

        		int coins = 0;
        		if (set.next()){
        			coins = set.getInt("coins");
        		}
        		
        		if (coins < 0){
        			coins = 0;
        			pst.close();

        			pst = conn.prepareStatement("DELETE FROM playerCoins WHERE playeruuid = ?");
        			pst.setBytes(1, bytes);
        			pst.executeUpdate();
        			conn.commit();
        			pst.close();
        		}
        		
        		return coins;
    		
    		} catch (SQLException ex){
                Main.log.severe(prefix + "Error on SQL grab coins. INFO:");
                Main.log.severe(prefix + "  " + ex.getMessage());
                return 0;
    		} finally {
    			
    			try {
    				if (set != null){
        				set.close();
        			}
        			if (pst != null){
        				pst.close();
        			}
    			} catch (SQLException ex){
                    Main.log.severe(prefix + "Error on SQL close of grabbing coins. INFO:");
                    Main.log.severe(prefix + "  " + ex.getMessage());
    			}	
    		}
    	} else {
    		return 0;
    	}
    	
    }
    
    
    public static void UpdateUUIDsWithCurrentNames(){
    	
    	Main.log.info(prefix + "Starting update of UUIDs with current usernames...");
    	long millis = System.currentTimeMillis();
    	
    	Statement st = null;
    	PreparedStatement prepared = null;
    	ResultSet set = null;
    	
    	try {
    		
    		Connection conn = connectToDB();
    		st = conn.createStatement();
    		set = st.executeQuery("SELECT playeruuid, playername FROM playerCoins");
    		
    		HashMap<byte[], String> toupdate = new HashMap<byte[], String>();
    		
    		HashMap<UUID, String> toprocess = new HashMap<UUID, String>();
    		
    		while (set.next()){
    			
    			byte[] bytes = set.getBytes("playeruuid");
    			String s = set.getString("playername");
    			if (bytes != null && s != null){
    				
    				toprocess.put(fromBytes(bytes), s);
    				
    			}	
    		}
    		
    		UUIDManager fetcher = new UUIDManager(new ArrayList<UUID>(toprocess.keySet()));
    		
    		Map<UUID, String> fetched = fetcher.call();
    		
    		if (!fetched.isEmpty()){
    			for (Entry<UUID, String> entry : fetched.entrySet()){
        			
        			String tocheck = toprocess.get(entry.getKey());
        			
        			if (tocheck != null && !tocheck.equals(entry.getValue())){
        				toupdate.put(toBytes(entry.getKey()), entry.getValue());
        			}
        			
        		}
    		}
    		
    		prepared = conn.prepareStatement("UPDATE playerCoins SET playername = ? WHERE playeruuid = ?");
    		
    		if (!toupdate.isEmpty()){
    			for (Entry<byte[], String> entry : toupdate.entrySet()){
        			
        			prepared.setString(1, entry.getValue());
        			prepared.setBytes(2, entry.getKey());
        			prepared.executeUpdate();
        			conn.commit();
        			
        		}
    		}
    		
    	} catch (SQLException ex){
            Main.log.severe(prefix + "Error on SQL update uuids with names. INFO:");
            Main.log.severe(prefix + "  " + ex.getMessage());

    	} catch (Exception ex){
    		
    		Main.log.severe(prefix + "Error on grabbing names from mojang database");
    		Main.log.severe(prefix + "  " + ex.getMessage());
    		
    	} finally {
    		
    		try {
    			
    			if (st != null){
    				st.close();
    			}
    			if (set != null){
    				set.close();
    			}
    			if (prepared != null){
    				prepared.close();
    			}
    			
    			long timetaken = System.currentTimeMillis() - millis;
    			Main.log.info(prefix + "Update finished. Took approx " + timetaken + " milliseconds (" + (timetaken / 1000L) + "s)!");
    			
    		} catch (SQLException ex){
               
    			Main.log.severe(prefix + "Error on SQL close of name-uuid updates. INFO:");
                Main.log.severe(prefix + "  " + ex.getMessage());

    		}
    	}
    }
    
    public static void updatePlayersName(Player p){
    	
       	PreparedStatement prepared = null;
    	ResultSet set = null;
    	
    	try {
    		
    		Connection conn = connectToDB();
    		prepared = conn.prepareStatement("SELECT playeruuid, playername FROM playerCoins WHERE playeruuid = ?");
    		
    		byte[] bytes = toBytes(p.getUniqueId());
    		prepared.setBytes(1, bytes);
    		
    		set = prepared.executeQuery();
    		
    		if (set.next()){
    			
    			String s = set.getString("playername");
    			if (!s.equals(p.getName())){
    				
    				prepared = null;
    				prepared = conn.prepareStatement("UPDATE playerCoins SET playername = ? WHERE playeruuid = ?");
    				prepared.setString(1, s);
    				prepared.setBytes(2, bytes);
    				
    			}
    			
    		}
    		
    	} catch (SQLException ex){
            Main.log.severe(prefix + "Error on SQL update uuid with name for " + p.getName() + ". INFO:");
            Main.log.severe(prefix + "  " + ex.getMessage());

    	} finally {
    		
    		try {
    			
    			if (set != null){
    				set.close();
    			}
    			if (prepared != null){
    				prepared.close();
    			}
    			    			
    		} catch (SQLException ex){
               
    			Main.log.severe(prefix + "Error on SQL close of name-uuid update for " + p.getName() + ". INFO:");
                Main.log.severe(prefix + "  " + ex.getMessage());

    		}
    	}
    	
    }
    
    public static byte[] toBytes(UUID uuid) {
        
    	ByteBuffer byteBuffer = ByteBuffer.wrap(new byte[16]);
        byteBuffer.putLong(uuid.getMostSignificantBits());
        byteBuffer.putLong(uuid.getLeastSignificantBits());
        return byteBuffer.array();
    
    }
    
    public static UUID fromBytes(byte[] array) {
        
    	if (array.length != 16) {
            throw new IllegalArgumentException("Illegal byte array length: " + array.length);
        }
     	ByteBuffer byteBuffer = ByteBuffer.wrap(array);
        long mostSignificant = byteBuffer.getLong();
        long leastSignificant = byteBuffer.getLong();
        return new UUID(mostSignificant, leastSignificant);
    
    }
    
}
