package net.amigocraft.tunneling.listeners;

import static net.amigocraft.tunneling.Main.wizardArenas;
import static net.amigocraft.tunneling.Main.wizardStages;

import java.util.ArrayList;
import java.util.List;

import net.amigocraft.mglib.api.ArenaFactory;
import net.amigocraft.mglib.api.MGPlayer;
import net.amigocraft.mglib.exception.ArenaExistsException;
import net.amigocraft.mglib.exception.ArenaNotExistsException;
import net.amigocraft.mglib.exception.InvalidLocationException;
import net.amigocraft.mglib.api.Location3D;
import net.amigocraft.tunneling.ConfigHandler;
import net.amigocraft.tunneling.Main;
import net.amigocraft.tunneling.SQLManager;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerMoveEvent;

public class PlayerListener implements Listener {

	@EventHandler(priority = EventPriority.MONITOR)
	public void onJoin(PlayerJoinEvent e){
		
		SQLManager.updatePlayersName(e.getPlayer());
		
	}
	
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onAsyncPlayerChat(AsyncPlayerChatEvent e){

		if (wizardStages.containsKey(e.getPlayer().getName())){
			if (!e.getMessage().startsWith("/")){
				if (e.getPlayer().hasPermission("arena.create")){
					Player p = e.getPlayer();
					String m = e.getMessage();
					if (m.equalsIgnoreCase("quit")){
						e.getPlayer().sendMessage(ChatColor.DARK_AQUA + "The wizard will now exit.");
						wizardStages.remove(e.getPlayer().getName());
						wizardArenas.remove(e.getPlayer().getName());
						return;
					}
					else if (m.equalsIgnoreCase("back")){
						wizardStages.put(e.getPlayer().getName(), wizardStages.get(e.getPlayer().getName()) - 1);
						e.getPlayer().sendMessage(ChatColor.DARK_AQUA + "You have been returned to the previous step.");
						return;
					}
					int stage = wizardStages.get(e.getPlayer().getName());
					switch (stage){
					case (0):{
						//TODO: Throw some easter eggs in here.
						e.setCancelled(true);
						try {
							Main.mg.getArenaFactory(e.getMessage());
							p.sendMessage(ChatColor.DARK_AQUA + "That arena already exists! Please type a different name.");
							break;
						}
						catch (ArenaNotExistsException ex){}
						wizardArenas.put(p.getName(), e.getMessage());
						e.getPlayer().sendMessage(ChatColor.DARK_AQUA + "The arena will be named \"" + e.getMessage() +
								"\". Is this okay? (yes/no)");
						wizardStages.put(p.getName(), 1);
						break;
					}
					case (1):{
						e.setCancelled(true);
						if (m.equalsIgnoreCase("yes")){
							p.sendMessage(ChatColor.DARK_AQUA + "Awesome! Next, please travel to the point which you wish to use as a player"
									+ "spawn and type \"ready\". You can use server chat in the meantime. :)");
							wizardStages.put(p.getName(), 2);
						}
						else if (m.equalsIgnoreCase("no")){
							p.sendMessage(ChatColor.DARK_AQUA + "Please type a new name for the arena.");
							wizardStages.put(p.getName(), 0);
						}
						else {
							p.sendMessage(ChatColor.DARK_AQUA + "That's not a valid response. Is the name \"" + wizardArenas.get(p.getName()) +
									"\" okay? (yes/no)");
						}
						break;
					}
					case (2):{
						if (m.equalsIgnoreCase("ready")){
							e.setCancelled(true);
							p.sendMessage(ChatColor.DARK_AQUA + "Great! {" + p.getLocation().getBlockX() + ", " +
									p.getLocation().getBlockY() + ", " + p.getLocation().getBlockZ() + "} in world \"" +
									p.getLocation().getWorld().getName() +
									"\" will be used as the player spawn.");
							try {
								Main.mg.createArena(wizardArenas.get(p.getName()), p.getLocation());
								p.sendMessage(ChatColor.DARK_AQUA + "Okay! Finally, please travel to the point which you wish to use"
										+ "as an infected spawn and type \"ready\". You can use server chat in the meantime. :)");
								wizardStages.put(p.getName(), 3);
							}
							catch (ArenaExistsException ex){
								ex.printStackTrace();
								p.sendMessage(ChatColor.RED + "The arena already exists! "
										+ "Did someone create it while you were in the wizard?");
								p.sendMessage(ChatColor.RED + "The wizard will not exit.");
							}
						}
						break;
					}
					case (3):{
						if (m.equalsIgnoreCase("ready")){
							e.setCancelled(true);
							try {
								Main.mg.getArenaFactory(wizardArenas.get(p.getName())).addSpawn(p.getLocation());
								p.sendMessage(ChatColor.DARK_AQUA + "Great! {" + p.getLocation().getBlockX() + ", " +
										p.getLocation().getBlockY() + ", " + p.getLocation().getBlockZ() + "} in world \"" +
										p.getLocation().getWorld().getName() +
										"\" will be used as the infected spawn.");
								p.sendMessage(ChatColor.DARK_AQUA + "Next, you'll need to define the finish line. "
										+ "Please travel to the first endpoint of the area you wish to use. All y-values are included "
										+ "in the area, so you need only mind the x and z coordinates. Type \"ready\" once you're there.");
								wizardStages.put(p.getName(), 4);
							}
							catch (ArenaNotExistsException ex){
								ex.printStackTrace();
								p.sendMessage(ChatColor.RED + "The arena no longer exists. The wizard will not exit.");
							}
							catch (InvalidLocationException ex){
								p.sendMessage(ChatColor.DARK_AQUA + "The infected spawn must be in the same world as the player spawn!");
								p.sendMessage(ChatColor.DARK_AQUA + "Please travel to a valid location and type \"ready\".");
							}
						}
						break;
					}
					case 4:{
						try {
							ArenaFactory af = Main.mg.getArenaFactory(wizardArenas.get(p.getName()));
							if (p.getWorld().getName().equals(af.getWorld())){
								af.setData("finish-line-1.x", p.getLocation().getBlockX());
								af.setData("finish-line-1.z", p.getLocation().getBlockZ());
								p.sendMessage(ChatColor.DARK_AQUA + "Okay! Now, please travel to the other endpoint nad type \"ready\" "
										+ "once you're there.");
								wizardStages.put(p.getName(), 5);
							}
							else {
								p.sendMessage(ChatColor.DARK_AQUA + "The finish line must be in the same world as the arena!");
								p.sendMessage(ChatColor.DARK_AQUA + "Please travel to a valid location and type \"ready\".");
								return;
							}
						}
						catch (ArenaNotExistsException ex){
							ex.printStackTrace();
							p.sendMessage(ChatColor.RED + "The arena no longer exists. The wizard will not exit.");
						}
						break;
					}
					case 5:{
						try {
							ArenaFactory af = Main.mg.getArenaFactory(wizardArenas.get(p.getName()));
							if (p.getWorld().getName().equals(af.getWorld())){
								af.setData("finish-line-2.x", p.getLocation().getBlockX());
								af.setData("finish-line-2.z", p.getLocation().getBlockZ());
								p.sendMessage(ChatColor.DARK_AQUA + "The arena has been created and saved to disk. "
										+ "You may now create a lobby for it.");
								p.sendMessage("The wizard will now exit.");
								wizardStages.remove(p.getName());
								wizardArenas.remove(p.getName());
							}
							else {
								p.sendMessage(ChatColor.DARK_AQUA + "The finish line must be in the same world as the arena!");
								p.sendMessage(ChatColor.DARK_AQUA + "Please travel to a valid location and type \"ready\".");
								return;
							}
						}
						catch (ArenaNotExistsException ex){
							ex.printStackTrace();
							p.sendMessage(ChatColor.RED + "The arena no longer exists. The wizard will not exit.");
						}
						break;
					}
					default:{
						e.setCancelled(true);
						p.sendMessage(ChatColor.RED + "You appear to have made your way to an invalid point in the wizard, meaning I as the "
								+ "developer messed something up. Please report this to me ASAP. :)");
						p.sendMessage(ChatColor.RED + "Your index is " + stage + ". Please take note of this. The wizard will now exit.");
						break;
					}
					}
				}
				else {
					e.getPlayer().sendMessage(ChatColor.RED + "Permission to create arenas has been revoked! The wizard will now exit.");
					wizardStages.remove(e.getPlayer().getName());
					wizardArenas.remove(e.getPlayer().getName());
				}
			}
		}
		else {
			MGPlayer sender = Main.mg.getMGPlayer(e.getPlayer().getName());
			if (sender != null){
				List<Player> remove = new ArrayList<Player>();
				for (Player p : e.getRecipients()){
					MGPlayer recipient = Main.mg.getMGPlayer(p.getName());
					if (!(recipient != null && sender.getArena().equals(recipient.getArena()) && sender.getTeam().equals(recipient.getTeam())))
						remove.add(p);
				}
				e.getRecipients().removeAll(remove);
			}
		}

	}

	@EventHandler
	public void onPlayerMove(PlayerMoveEvent e){
		if (ConfigHandler.FORCE_FREEZE && MGListener.frozen.contains(e.getPlayer().getName()))
			if (!Location3D.valueOf(e.getFrom()).equals(Location3D.valueOf(e.getTo())))
				e.getPlayer().teleport(new Location(e.getFrom().getWorld(), e.getFrom().getX(), e.getFrom().getY(), e.getFrom().getZ(),
						e.getTo().getPitch(), e.getTo().getYaw()));
	}

	@EventHandler
	public void onPlayerInteract(PlayerInteractEvent e){
		if (Main.mg.isPlayer(e.getPlayer().getName())){
			if (e.getItem() != null){
				String itemName = e.getItem().getType().toString();
				if (itemName.contains("SWORD") ||
						itemName.contains("PICKAXE") ||
						itemName.contains("SPADE") ||
						itemName.contains("HOE") || 
						itemName.contains("AXE") ||
						itemName.equals("FISHING_ROD") ||
						itemName.equals("FLINT_AND_STEEL"))
					e.getItem().setDurability((short)0);
			}
		}
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void onEntityDamageByEntity(EntityDamageByEntityEvent e){
		if (e.getEntity().getType() == EntityType.PLAYER &&
				(e.getDamager().getType() == EntityType.PLAYER ||
				(e.getDamager() instanceof Projectile && ((Projectile)e.getDamager()).getShooter() instanceof Player))){
			if ((e.getDamager().getType() == EntityType.PLAYER && Main.mg.isPlayer(((Player)e.getDamager()).getName()) ||
					Main.mg.isPlayer(((Player)((Projectile)e.getDamager()).getShooter()).getName()))){
				if (e.getFinalDamage() >= ((LivingEntity)e.getEntity()).getHealth()){
					MGPlayer p = e.getDamager() instanceof Player ? Main.mg.getMGPlayer(((Player)e.getDamager()).getName()) :
						Main.mg.getMGPlayer(((Player)((Projectile)e.getDamager()).getShooter()).getName());
					p.setMetadata("kills", (int)p.getMetadata("kills") + 1);
				}
			}
		}
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPlayerDropItem(PlayerDropItemEvent e){
		if (Main.mg.isPlayer(e.getPlayer().getName()))
			e.setCancelled(true);
	}

}
