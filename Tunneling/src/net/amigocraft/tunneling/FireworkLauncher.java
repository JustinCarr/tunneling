package net.amigocraft.tunneling;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;
import org.bukkit.Color;
import org.bukkit.FireworkEffect;
import org.bukkit.Location;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Firework;
import org.bukkit.inventory.meta.FireworkMeta;

public class FireworkLauncher
{
  private Location location;
  private Random random;

  public FireworkLauncher(Location location)
  {
    this.location = location;
    this.random = new Random();
  }

  public Location getDatLocation() {
    return this.location;
  }

  public void launchRandomFireWork() {
    Firework fw = (Firework)this.location.getWorld().spawnEntity(this.location, EntityType.FIREWORK);
    FireworkMeta meta = fw.getFireworkMeta();

    meta.setPower(1 + this.random.nextInt(4));
    FireworkEffect.Builder builder = FireworkEffect.builder()
      .trail(this.random.nextBoolean())
      .flicker(this.random.nextBoolean());

    builder.with(org.bukkit.FireworkEffect.Type.values()[this.random.nextInt(org.bukkit.FireworkEffect.Type.values().length)]);
    Set<Color> colors = new HashSet<Color>();
    for (int i = 0; i < 3; i++) {
      colors.add(Color.fromRGB(this.random.nextInt(256), this.random.nextInt(256), this.random.nextInt(256)));
    }

    builder.withColor(colors);
    meta.addEffect(builder.build());
    fw.setFireworkMeta(meta);
  }
}