package net.amigocraft.tunneling;

public enum Kit {

	standard("", 1);
	
	private String dispname;
	private int costtobuy;
	
	private Kit(String display_name, int cost){
		this.dispname = display_name;
		this.costtobuy = cost;
	}
	
	public int getCost(){
		return this.costtobuy;
	}
	
	public String getDisplayName(){
		return this.dispname;
	}
	
}
